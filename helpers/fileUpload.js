var multer = require('multer');
var storage =   multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, __dirname + '/../uploads');
  },
  filename: function (req, file, callback) {
    callback(null, file.fieldname + '-' + Date.now());
  }
});
var uploadFile = multer({ storage : storage}).single('photo');
module.exports = {
	upload: function(req, res, callback){
		uploadFile(req, res, function(err){
			if(err) {
            	return callback(err);
        	}
        	return callback(null, req);
		});
	}
};