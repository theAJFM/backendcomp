var sortTotal = function(a, b){
	return parseFloat(b.doc.total) - parseFloat(a.doc.total);
};
module.exports = {
	calculateOli: function(min_harga, max_viskositas, max_ketahanan, harga_p, viskositas_p, ketahanan_p, result, fields){
		CONST = fields;
		for(var i=0;i<result.total_rows;i++){
			harga_t = min_harga/result.rows[i].doc.harga*(harga_p/100);
			viskositas_t = parseFloat(CONST.viskositas[result.rows[i].doc.viskositas])/max_viskositas*(viskositas_p/100);
			ketahanan_t = parseFloat(CONST.ketahanan[result.rows[i].doc.ketahanan])/max_ketahanan*(ketahanan_p/100);
			result.rows[i].doc.total = +((harga_t + viskositas_t + ketahanan_t).toFixed(3));
		}
		result.rows.sort(sortTotal);
		return result;
	},
	calculateCat: function(min_harga, min_daya_kering, max_daya_sebar, harga_p, daya_kering_p, daya_sebar_p, daya_tahan_p, daya_kilap_p, result, fields){
		CONST = fields;
		//max_total = 0;
		for(var i=0;i<result.total_rows;i++){
			harga_t = min_harga/result.rows[i].doc.harga*(harga_p/100);
			daya_kering_t = min_daya_kering/result.rows[i].doc.daya_kering*(daya_kering_p/100);
			daya_sebar_t = result.rows[i].doc.daya_sebar/max_daya_sebar*(daya_sebar_p/100);
			daya_kilap_t = parseFloat(CONST.daya_kilap[result.rows[i].doc.daya_kilap])*(daya_kilap_p/100);
			daya_tahan_t = parseFloat(CONST.daya_tahan_cat[result.rows[i].doc.daya_tahan_cat])*(daya_tahan_p/100);
			result.rows[i].doc.total = +((harga_t + daya_kering_t + daya_sebar_t + daya_kilap_t + daya_tahan_t).toFixed(3));
			//max_total = Math.max(max_total, result.rows[i].doc.total);
		}
		/*var res = {
			total_rows : 0,
			rows : []
		};
		for(var i=0;i<result.total_rows;i++){
			if(result.rows[i].doc.total == max_total){
				res.rows.push(result.rows[i]);
				res.total_rows++;
			}
		}*/
		result.rows.sort(sortTotal);
		return result;
	},
	calculateAki: function(min_harga, max_kapasitas, max_tegangan, max_CCA, harga_p, kapasitas_p, tegangan_p, CCA_p, result){
		for(var i=0;i<result.total_rows;i++){
			harga_t = min_harga/result.rows[i].doc.harga*(harga_p/100);
			kapasitas_t = result.rows[i].doc.kapasitas/max_kapasitas*(kapasitas_p/100);
			tegangan_t = result.rows[i].doc.tegangan/max_tegangan*(tegangan_p/100);
			CCA_t = result.rows[i].doc.cca/max_CCA*(CCA_p/100);
			result.rows[i].doc.total = +((harga_t + kapasitas_t + tegangan_t + CCA_t).toFixed(3));
			//max_total = Math.max(max_total, result.rows[i].doc.total);
		}
		result.rows.sort(sortTotal);
		return result;
	},
	calculateMinyakRem: function(min_harga, max_boiling_point, max_wet_boiling_point, harga_p, boiling_point_p, wet_boiling_point_p, result){
		for(var i=0;i<result.total_rows;i++){
			harga_t = min_harga/result.rows[i].doc.harga*(harga_p/100);
			boiling_point_t = result.rows[i].doc.boiling_point/max_boiling_point*(boiling_point_p/100);
			wet_boiling_point_t = result.rows[i].doc.wet_boiling_point/max_wet_boiling_point*(wet_boiling_point_p/100);
			result.rows[i].doc.total = +((harga_t + boiling_point_t + wet_boiling_point_t).toFixed(3));
		}
		result.rows.sort(sortTotal);
		return result;
	},

	calculateKacaFilm: function(min_harga, max_VLT, max_UVR, max_IRR, max_TSER, harga_p, VLT_p, UVR_p, IRR_p, TSER_p, result){
		for(var i=0;i<result.total_rows;i++){
			harga_t = min_harga/result.rows[i].doc.harga*(harga_p/100);
			VLT_t = result.rows[i].doc.VLT/max_VLT*(VLT_p/100);
			UVR_t = result.rows[i].doc.UVR/max_UVR*(UVR_p/100);
			IRR_t = result.rows[i].doc.IRR/max_IRR*(IRR_p/100);
			TSER_t = result.rows[i].doc.TSER/max_TSER*(TSER_p/100);
			result.rows[i].doc.total = +((harga_t + VLT_t + UVR_t + IRR_t + TSER_t).toFixed(3));
		}
		result.rows.sort(sortTotal);
		return result;
	},
	calculateCatVernis: function(min_harga, max_daya_tahan_kilap, min_daya_pengeringan, harga_p, daya_tahan_kilap_p, daya_pengeringan_p, result, fields){
		CONST = fields;
		for(var i=0;i<result.total_rows;i++){
			harga_t = min_harga/result.rows[i].doc.harga*(harga_p/100);
			daya_tahan_kilap_t = parseFloat(CONST.daya_tahan_kilap[result.rows[i].doc.daya_tahan_kilap])/max_daya_tahan_kilap*(daya_tahan_kilap_p/100);
			daya_pengeringan_t = min_daya_pengeringan/result.rows[i].doc.daya_pengeringan*(daya_pengeringan_p/100);
			result.rows[i].doc.total = +((harga_t + daya_tahan_kilap_t + daya_pengeringan_t).toFixed(3));
		}
		result.rows.sort(sortTotal);
		return result;
	},
	calculateBohlam : function(min_harga, max_voltase, max_daya_power, max_daya_power_jauh, max_daya_power_dekat, max_pancaran_cahaya, harga_p, voltase_p, daya_power_p, daya_power_jauh_p, daya_power_dekat_p, pancaran_cahaya_p, result){
		if(result.total_rows === 0){
			return result;
		}
		var sample = result.rows[0];
		if(sample.doc.jenis_bohlam == "Lampu Depan"){
			if(sample.ukuran_bohlam == "H4"){
				for(var i=0;i<result.total_rows;i++){
					harga_t = min_harga/result.rows[i].doc.harga*(harga_p/100);
					voltase_t = result.rows[i].doc.voltase/max_voltase*(voltase_p/100);
					daya_power_jauh_t = result.rows[i].doc.daya_power_jauh/max_daya_power_jauh*(daya_power_jauh_p/100);
					daya_power_dekat_t = result.rows[i].doc.daya_power_dekat/max_daya_power_dekat*(daya_power_dekat_p/100);
					pancaran_cahaya_t = result.rows[i].doc.pancaran_cahaya/max_pancaran_cahaya*(pancaran_cahaya_p/100);
					result.rows[i].doc.total = +((harga_t + voltase_t + daya_power_jauh_t + daya_power_dekat_t + pancaran_cahaya_t).toFixed(3));
				}
			}
			else{
				for(var i=0;i<result.total_rows;i++){
					harga_t = min_harga/result.rows[i].doc.harga*(harga_p/100);
					voltase_t = result.rows[i].doc.voltase/max_voltase*(voltase_p/100);
					daya_power_t = result.rows[i].doc.daya_power/max_daya_power*(daya_power_p/100);
					pancaran_cahaya_t = result.rows[i].doc.pancaran_cahaya/max_pancaran_cahaya*(pancaran_cahaya_p/100);
					result.rows[i].doc.total = +((harga_t + voltase_t + daya_power_t + pancaran_cahaya_t).toFixed(3));
				}
			}
		}
		else{
			for(var i=0;i<result.total_rows;i++){
				harga_t = min_harga/result.rows[i].doc.harga*(harga_p/100);
				voltase_t = result.rows[i].doc.voltase/max_voltase*(voltase_p/100);
				daya_power_t = result.rows[i].doc.daya_power/max_daya_power*(daya_power_p/100);
				result.rows[i].doc.total = +((harga_t + voltase_t + daya_power_t).toFixed(3));
			}
		}
		result.rows.sort(sortTotal);
		return result;
	},
	sorted: function(a, b){
		var first = a.doc.kode_produk.slice(1);
		var second = b.doc.kode_produk.slice(1);
		return parseInt(first) - parseInt(second);
	}
};