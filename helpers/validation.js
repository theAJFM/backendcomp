module.exports = {
	oliErrors : function(body){
		var errors = [];

		if(body.nama_produk === ''){
			errors.push("Nama produk tidak bisa kosong");
		}

		if(body.harga === ''){
			errors.push("Harga tidak bisa kosong");
		}

		if(isNaN(body.harga)){
			errors.push("Harga harus numerik");
		}

		return errors;
	},
	catErrors : function(body){
		var errors = [];

		if(body.nama_produk === ''){
			errors.push("Nama produk tidak bisa kosong");
		}

		if(body.harga === ''){
			errors.push("Harga tidak bisa kosong");
		}

		if(isNaN(body.harga)){
			errors.push("Harga harus numerik");
		}

		if(body.asal_produk === ''){
			errors.push("Asal produk tidak bisa kosong");
		}

		if(typeof body.warna_cat === "undefined"){
			errors.push("Warna cat tidak bisa kosong");
		}

		if(body.daya_kering === ''){
			errors.push("Daya kering tidak bisa kosong");
		}

		if(isNaN(body.daya_kering)){
			errors.push("Daya kering harus numerik");
		}

		if(body.daya_sebar === ''){
			errors.push("Daya sebar tidak bisa kosong");
		}

		if(isNaN(body.daya_sebar)){
			errors.push("Daya sebar harus numerik");
		}

		return errors;
	},
	akiErrors : function(body){
		var errors = [];

		if(body.nama_produk === ''){
			errors.push("Nama produk tidak bisa kosong");
		}

		if(body.harga === ''){
			errors.push("Harga tidak bisa kosong");
		}

		if(isNaN(body.harga)){
			errors.push("Harga harus numerik");
		}

		if(typeof body.silinder_mesin === "undefined"){
			errors.push("Silinder Mesin tidak bisa kosong");
		}

		if(body.kapasitas === ''){
			errors.push("Kapasitas tidak bisa kosong");
		}

		if(isNaN(body.kapasitas)){
			errors.push("Kapasitas harus numerik");
		}

		if(body.tegangan === ''){
			errors.push("Tegangan tidak bisa kosong");
		}

		if(isNaN(body.tegangan)){
			errors.push("Tegangan harus numerik");
		}

		if(body.cca === ''){
			errors.push("CCA tidak bisa kosong");
		}

		if(isNaN(body.cca)){
			errors.push("CCA harus numerik");
		}

		return errors;
	},

	minyakRemErrors : function(body){
		var errors = [];

		if(body.nama_produk === ''){
			errors.push("Nama produk tidak bisa kosong");
		}

		if(body.harga === ''){
			errors.push("Harga tidak bisa kosong");
		}

		if(isNaN(body.harga)){
			errors.push("Harga harus numerik");
		}

		if(body.boiling_point === ''){
			errors.push("Boiling point tidak bisa kosong");
		}

		if(isNaN(body.boiling_point)){
			errors.push("Boiling point harus numerik");
		}

		if(body.wet_boiling_point === ''){
			errors.push("Wet boiling point tidak bisa kosong");
		}

		if(isNaN(body.wet_boiling_point)){
			errors.push("Wet boiling point harus numerik");
		}

		console.log(errors);
		return errors;
	},

	kacaFilmErrors : function(body){
		var errors = [];

		if(body.nama_produk === ''){
			errors.push("Nama produk tidak bisa kosong");
		}

		if(body.harga === ''){
			errors.push("Harga tidak bisa kosong");
		}

		if(isNaN(body.harga)){
			errors.push("Harga harus numerik");
		}

		if(typeof body.basis_warna === "undefined"){
			errors.push("Basis warna tidak bisa kosong");
		}

		if(body.tipe === ''){
			errors.push("Tipe tidak bisa kosong");
		}

		if(body.VLT === ''){
			errors.push("VLT tidak bisa kosong");
		}

		if(isNaN(body.VLT)){
			errors.push("VLT harus numerik");
		}

		if(body.UVR === ''){
			errors.push("UVR tidak bisa kosong");
		}

		if(isNaN(body.UVR)){
			errors.push("UVR harus numerik");
		}

		if(body.IRR === ''){
			errors.push("IRR tidak bisa kosong");
		}

		if(isNaN(body.IRR)){
			errors.push("IRR harus numerik");
		}

		if(body.TSER === ''){
			errors.push("TSER tidak bisa kosong");
		}

		if(isNaN(body.TSER)){
			errors.push("TSER harus numerik");
		}

		return errors;
	},

	catVernisErrors : function(body){
		var errors = [];

		if(body.nama_produk === ''){
			errors.push("Nama produk tidak bisa kosong");
		}

		if(body.harga === ''){
			errors.push("Harga tidak bisa kosong");
		}

		if(isNaN(body.harga)){
			errors.push("Harga harus numerik");
		}

		if(body.asal_produk === ''){
			errors.push("Asal produk tidak bisa kosong");
		}

		if(body.daya_pengeringan === ''){
			errors.push("Daya pengeringan tidak bisa kosong");
		}

		if(isNaN(body.daya_pengeringan)){
			errors.push("Daya pengeringan harus numerik");
		}

		return errors;
	},

	bohlamErrors : function(body){
		var errors = [];

		if(body.nama_produk === ''){
			errors.push("Nama produk tidak bisa kosong");
		}

		if(body.harga === ''){
			errors.push("Harga tidak bisa kosong");
		}

		if(isNaN(body.harga)){
			errors.push("Harga harus numerik");
		}

		if(body.warna_cahaya === ''){
			errors.push("Warna cahaya tidak bisa kosong");
		}

		if(body.jenis_bohlam == "Lampu Depan"){
			if(body.ukuran_bohlam == "H4"){
				if(body.daya_power_dekat === ''){
					errors.push("Daya power dekat tidak bisa kosong");
				}

				if(isNaN(body.daya_power_dekat)){
					errors.push("Daya power dekat harus numerik");
				}

				if(body.daya_power_jauh === ''){
					errors.push("Daya power jauh tidak bisa kosong");
				}

				if(isNaN(body.daya_power_jauh)){
					errors.push("Daya power jauh harus numerik");
				}
			}
			else{
				if(body.daya_power === ''){
					errors.push("Daya power tidak bisa kosong");
				}

				if(isNaN(body.daya_power)){
					errors.push("Daya power harus numerik");
				}
			}

			if(body.daya_pancaran_cahaya === ''){
				errors.push("Pancaran cahaya tidak bisa kosong");
			}

			if(isNaN(body.pancaran_cahaya)){
				errors.push("Pancaran cahaya harus numerik");
			}
		}
		else{
			if(body.daya_power === ''){
				errors.push("Daya power tidak bisa kosong");
			}

			if(isNaN(body.daya_power)){
				errors.push("Daya power harus numerik");
			}
		}

		if(body.voltase === ''){
			errors.push("Voltase tidak bisa kosong");
		}

		if(isNaN(body.voltase)){
			errors.push("Voltase harus numerik");
		}

		return errors;
	},
	oliKriteriaErrors : function(body){
		var errors = [];
		var total = parseFloat(body.kriteria_harga) + parseFloat(body.kriteria_viskositas) + parseFloat(body.kriteria_ketahanan);

		if(body.kriteria_harga === ''){
			errors.push("Kriteria harga tidak bisa kosong  ");
		}

		if(isNaN(body.kriteria_harga)){
			errors.push("Kriteria harga harus numerik  ");
		}

		if(body.kriteria_viskositas === ''){
			errors.push("Kriteria viskositas tidak bisa kosong  ");
		}

		if(isNaN(body.kriteria_viskositas)){
			errors.push("Kriteria viskositas harus numerik  ");
		}

		if(body.kriteria_ketahanan === ''){
			errors.push("Kriteria ketahanan tidak bisa kosong  ");
		}

		if(isNaN(body.kriteria_ketahanan)){
			errors.push("Kriteria ketahanan harus numerik  ");
		}

		if(body.tahun_produksi === ''){
			errors.push("Kriteria tahun produksi tidak bisa kosong  ");
		}

		if(total != 100){
			errors.push("Total kriteria harus 100  ");
		}

		return errors;
	},
	catKriteriaErrors : function(body){
		var errors = [];
		var total = parseFloat(body.kriteria_harga) + parseFloat(body.kriteria_daya_sebar) + parseFloat(body.kriteria_daya_kering) + parseFloat(body.kriteria_daya_kilap) + parseFloat(body.kriteria_daya_tahan);
		if(typeof body.warna_cat === "undefined"){
			errors.push("Warna cat tidak bisa kosong  ");
		}

		if(body.kriteria_harga === ''){
			errors.push("Kriteria harga tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_harga)){
			errors.push("Kriteria harga harus numerik   ");
		}

		if(body.kriteria_daya_tahan === ''){
			errors.push("Kriteria daya kilap tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_daya_tahan)){
			errors.push("Kriteria daya kilap harus numerik   ");
		}

		if(body.kriteria_daya_kilap === ''){
			errors.push("Kriteria daya kilap tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_daya_kilap)){
			errors.push("Kriteria daya kilap harus numerik   ");
		}

		if(body.kriteria_daya_kering === ''){
			errors.push("Kriteria daya kering tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_daya_kering)){
			errors.push("Kriteria daya kering harus numerik   ");
		}

		if(body.kriteria_daya_sebar === ''){
			errors.push("Kriteria daya sebar tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_daya_sebar)){
			errors.push("Kriteria daya sebar harus numerik   ");
		}

		if(total != 100){
			errors.push("Total kriteria harus 100   ");
		}

		return errors;
	},
	akiKriteriaErrors : function(body){
		var errors = [];
		var total = parseFloat(body.kriteria_harga) + parseFloat(body.kriteria_kapasitas) + parseFloat(body.kriteria_tegangan) + parseFloat(body.kriteria_CCA);
		if(typeof body.silinder_mesin === "undefined"){
			errors.push("Silinder mesin tidak bisa kosong   ");
		}

		if(body.kriteria_harga === ''){
			errors.push("Kriteria harga tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_harga)){
			errors.push("Kriteria harga harus numerik   ");
		}

		if(body.kriteria_kapasitas === ''){
			errors.push("Kriteria kapasitas tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_kapasitas)){
			errors.push("Kriteria kapasitas harus numerik   ");
		}

		if(body.kriteria_tegangan === ''){
			errors.push("Kriteria tegangan tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_tegangan)){
			errors.push("Kriteria tegangan harus numerik   ");
		}

		if(body.kriteria_CCA === ''){
			errors.push("Kriteria CCA tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_CCA)){
			errors.push("Kriteria CCA harus numerik   ");
		}

		if(total != 100){
			errors.push("Total kriteria harus 100   ");
		}

		return errors;
	},
	minyakRemKriteriaErrors : function(body){
		var errors = [];
		var total = parseFloat(body.kriteria_harga) + parseFloat(body.kriteria_boiling_point) + parseFloat(body.kriteria_wet_boiling_point);
		
		if(typeof body.DOT === "undefined"){
			errors.push("DOT tidak bisa kosong   ");
		}

		if(body.kriteria_harga === ''){
			errors.push("Kriteria harga tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_harga)){
			errors.push("Kriteria harga harus numerik   ");
		}

		if(body.kriteria_boiling_point === ''){
			errors.push("Kriteria boiling point tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_boiling_point)){
			errors.push("Kriteria boiling point harus numerik   ");
		}

		if(body.kriteria_wet_boiling_point === ''){
			errors.push("Kriteria wet boiling point tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_wet_boiling_point)){
			errors.push("Kriteria wet boiling point harus numerik   ");
		}

		if(total != 100){
			errors.push("Total kriteria harus 100   ");
		}

		return errors;
	},
	kacaFilmKriteriaErrors : function(body){
		var errors = [];
		var total = parseFloat(body.kriteria_harga) + parseFloat(body.kriteria_VLT) + parseFloat(body.kriteria_UVR) + parseFloat(body.kriteria_IRR) + parseFloat(body.kriteria_TSER);
		
		if(typeof body.kegelapan === "undefined"){
			errors.push("Kegelapan tidak bisa kosong   ");
		}

		if(body.kriteria_harga === ''){
			errors.push("Kriteria harga tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_harga)){
			errors.push("Kriteria harga harus numerik   ");
		}

		if(body.kriteria_VLT === ''){
			errors.push("Kriteria VLT tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_VLT)){
			errors.push("Kriteria VLT harus numerik   ");
		}

		if(body.kriteria_UVR === ''){
			errors.push("Kriteria UVR tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_UVR)){
			errors.push("Kriteria UVR harus numerik   ");
		}

		if(body.kriteria_IRR === ''){
			errors.push("Kriteria IRR tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_IRR)){
			errors.push("Kriteria IRR harus numerik   ");
		}

		if(body.kriteria_TSER === ''){
			errors.push("Kriteria TSER tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_TSER)){
			errors.push("Kriteria TSER harus numerik   ");
		}

		if(total != 100){
			errors.push("Total kriteria harus 100   ");
		}

		return errors;
	},
	catVernisKriteriaErrors : function(body){
		var errors = [];
		var total = parseFloat(body.kriteria_harga) + parseFloat(body.kriteria_daya_tahan_kilap) + parseFloat(body.kriteria_pengeringan);

		if(body.kriteria_harga === ''){
			errors.push("Kriteria harga tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_harga)){
			errors.push("Kriteria harga harus numerik   ");
		}

		if(body.kriteria_daya_tahan_kilap === ''){
			errors.push("Kriteria daya tahan & kilap tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_daya_tahan_kilap)){
			errors.push("Kriteria daya tahan & kilap harus numerik   ");
		}

		if(body.kriteria_pengeringan === ''){
			errors.push("Kriteria daya pengeringan tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_pengeringan)){
			errors.push("Kriteria daya pengeringan harus numerik   ");
		}

		if(total != 100){
			errors.push("Total kriteria harus 100   ");
		}

		return errors;
	},
	bohlamKriteriaErrors : function(body){
		var errors = [];
		var total = parseFloat(body.kriteria_harga) + parseFloat(body.kriteria_voltase);

		if(body.kriteria_harga === ''){
			errors.push("Harga tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_harga)){
			errors.push("Harga harus numerik   ");
		}

		if(body.jenis_bohlam == "Lampu Depan"){

			if(body.ukuran_bohlam == "H4"){
				if(body.kriteria_daya_power_dekat === ''){
					errors.push("Kriteria daya power dekat tidak bisa kosong   ");
				}

				if(isNaN(body.kriteria_daya_power_dekat)){
					errors.push("Kriteria daya power dekat harus numerik   ");
				}

				if(body.kriteria_daya_power_jauh === ''){
					errors.push("Kriteria daya power jauh tidak bisa kosong   ");
				}

				if(isNaN(body.kriteria_daya_power_jauh)){
					errors.push("Kriteria daya power jauh harus numerik   ");
				}

				total += parseFloat(body.kriteria_daya_power_dekat) + parseFloat(body.kriteria_daya_power_jauh);
			}
			else{
				if(body.kriteria_daya_power === ''){
					errors.push("Kriteria daya power tidak bisa kosong   ");
				}

				if(isNaN(body.kriteria_daya_power)){
					errors.push("Kriteria daya power harus numerik   ");
				}

				total += parseFloat(body.kriteria_daya_power);
			}

			if(body.kriteria_pancaran_cahaya === ''){
				errors.push("Kriteria pancaran cahaya tidak bisa kosong   ");
			}

			if(isNaN(body.kriteria_pancaran_cahaya)){
				errors.push("Kriteria pancaran cahaya harus numerik   ");
			}

			total += parseFloat(body.kriteria_pancaran_cahaya);
		}
		else{
			if(body.kriteria_daya_power === ''){
				errors.push("Kriteria daya power tidak bisa kosong   ");
			}

			if(isNaN(body.kriteria_daya_power)){
				errors.push("Kriteria daya power harus numerik   ");
			}

			total += parseFloat(body.kriteria_daya_power);			
		}

		if(body.kriteria_voltase === ''){
			errors.push("Kriteria voltase tidak bisa kosong   ");
		}

		if(isNaN(body.kriteria_voltase)){
			errors.push("Kriteria voltase harus numerik   ");
		}

		if(total != 100){
			errors.push("Total kriteria harus 100   ");
		}

		return errors;
	}
};