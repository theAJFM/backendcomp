jQuery(window).load(function (){
	jQuery('.content').load('/output_oli', function() {
	});
});

jQuery('#output_form').submit(function(e){
	var url = jQuery('#output_form').attr('action');
	jQuery.ajax({
		type: "POST",
        url: url,
        data: jQuery(this).serialize(),
        success: function(data){
        	if(Array.isArray(data)){
        		alert(data); 
        	}
        	else{
        		jQuery('.result').html(data);
        	}
        }
    });
	e.preventDefault(); 
});
function pilihJenisProduk(obj){
	var option = obj.value;
	var action;
	var page;
	if(option == 1){
		page = 'output_oli';
		action = 'find_oli';
	}
	else if(option == 2){
		page = 'output_cat';
		action = 'find_cat';
	}
	else if(option == 3){
		page = 'output_aki';
		action = 'find_aki';
	}
	else if(option == 4){
		page = 'output_minyak_rem';
		action = 'find_minyak_rem';
	}
	else if(option == 5){
		page = 'output_kaca_film';
		action = 'find_kaca_film';
	}
	else if(option == 6){
		page = 'output_cat_vernis';
		action = 'find_cat_vernis';
	}
	else if(option == 7){
		page = 'output_bohlam';
		action = 'find_bohlam';
	}

	jQuery('.content').load('/'+page, function() {
		jQuery('select#jenis_produk').val(option);
		jQuery('#output_form').attr('action', action);
	});
}

function extraByJenisBohlam(obj){
	var option = obj.value;
	if(option == "Lampu Depan"){
		jQuery('#ukuran-bohlam').attr('style', 'display:table-row');
		jQuery('#pancaran-cahaya').attr('style', 'display:table-row');
	}
	else{
		jQuery('#ukuran-bohlam').attr('style', 'display:none');
		jQuery('#pancaran-cahaya').attr('style', 'display:none');
		jQuery('#daya-power').attr('style', 'display:table-row');
		jQuery('#daya-power-dekat').attr('style', 'display:none');
		jQuery('#daya-power-jauh').attr('style', 'display:none');
	}
}

function extraByUkuranBohlam(obj){
	var option = obj.value;
	if(option == "H4"){
		jQuery('#daya-power').attr('style', 'display:none');
		jQuery('#daya-power-dekat').attr('style', 'display:table-row');
		jQuery('#daya-power-jauh').attr('style', 'display:table-row');
	}
	else{
		jQuery('#daya-power').attr('style', 'display:table-row');
		jQuery('#daya-power-dekat').attr('style', 'display:none');
		jQuery('#daya-power-jauh').attr('style', 'display:none');
	}
}

function totalOli(){
	var harga = parseFloat(jQuery('#oli_harga').val());
	var viskositas = parseFloat(jQuery('#oli_viskositas').val());
	var ketahanan = parseFloat(jQuery('#oli_ketahanan').val());
	var total = harga + viskositas + ketahanan;
	if(isNaN(total)){
		jQuery('#total').html("Total is not valid!").addClass('text-danger').removeClass('text-success text-warning');
	}
	else if(total == 100){
		jQuery('#total').html("Total is " + total).addClass('text-success').removeClass('text-danger text-warning');
	}
	else{
		jQuery('#total').html("Total is " + total).addClass('text-warning').removeClass('text-danger text-success');
	}
}

function totalCat(){
	var harga = parseFloat(jQuery('#cat_harga').val());
	var daya_kilap = parseFloat(jQuery('#cat_daya_kilap').val());
	var daya_tahan = parseFloat(jQuery('#cat_daya_tahan').val());
	var daya_kering = parseFloat(jQuery('#cat_daya_kering').val());
	var daya_sebar = parseFloat(jQuery('#cat_daya_sebar').val());
	var total = harga + daya_kilap + daya_tahan + daya_kering + daya_sebar;
	if(isNaN(total)){
		jQuery('#total').html("Total is not valid!").addClass('text-danger').removeClass('text-success text-warning');
	}
	else if(total == 100){
		jQuery('#total').html("Total is " + total).addClass('text-success').removeClass('text-danger text-warning');
	}
	else{
		jQuery('#total').html("Total is " + total).addClass('text-warning').removeClass('text-danger text-success');
	}
}

function totalAki(){
	var harga = parseFloat(jQuery('#aki_harga').val());
	var kapasitas = parseFloat(jQuery('#aki_kapasitas').val());
	var tegangan = parseFloat(jQuery('#aki_tegangan').val());
	var CCA = parseFloat(jQuery('#aki_CCA').val());
	var total = harga + kapasitas + tegangan + CCA;
	if(isNaN(total)){
		jQuery('#total').html("Total is not valid!").addClass('text-danger').removeClass('text-success text-warning');
	}
	else if(total == 100){
		jQuery('#total').html("Total is " + total).addClass('text-success').removeClass('text-danger text-warning');
	}
	else{
		jQuery('#total').html("Total is " + total).addClass('text-warning').removeClass('text-danger text-success');
	}
}

function totalMinyakRem(){
	var harga = parseFloat(jQuery('#minyak_rem_harga').val());
	var boiling_point = parseFloat(jQuery('#minyak_rem_boiling_point').val());
	var wet_boiling_point = parseFloat(jQuery('#minyak_rem_wet_boiling_point').val());
	var total = harga + boiling_point + wet_boiling_point;
	if(isNaN(total)){
		jQuery('#total').html("Total is not valid!").addClass('text-danger').removeClass('text-success text-warning');
	}
	else if(total == 100){
		jQuery('#total').html("Total is " + total).addClass('text-success').removeClass('text-danger text-warning');
	}
	else{
		jQuery('#total').html("Total is " + total).addClass('text-warning').removeClass('text-danger text-success');
	}
}

function totalKacaFilm(){
	var harga = parseFloat(jQuery('#kaca_film_harga').val());
	var VLT = parseFloat(jQuery('#kaca_film_VLT').val());
	var UVR = parseFloat(jQuery('#kaca_film_UVR').val());
	var IRR = parseFloat(jQuery('#kaca_film_IRR').val());
	var TSER = parseFloat(jQuery('#kaca_film_TSER').val());
	var total = harga + VLT + UVR + IRR + TSER;
	if(isNaN(total)){
		jQuery('#total').html("Total is not valid!").addClass('text-danger').removeClass('text-success text-warning');
	}
	else if(total == 100){
		jQuery('#total').html("Total is " + total).addClass('text-success').removeClass('text-danger text-warning');
	}
	else{
		jQuery('#total').html("Total is " + total).addClass('text-warning').removeClass('text-danger text-success');
	}
}

function totalCatVernis(){
	var harga = parseFloat(jQuery('#cat_vernis_harga').val());
	var daya_tahan_kilap = parseFloat(jQuery('#cat_vernis_daya_tahan_kilap').val());
	var pengeringan = parseFloat(jQuery('#cat_vernis_pengeringan').val());
	var total = harga + daya_tahan_kilap + pengeringan;
	if(isNaN(total)){
		jQuery('#total').html("Total is not valid!").addClass('text-danger').removeClass('text-success text-warning');
	}
	else if(total == 100){
		jQuery('#total').html("Total is " + total).addClass('text-success').removeClass('text-danger text-warning');
	}
	else{
		jQuery('#total').html("Total is " + total).addClass('text-warning').removeClass('text-danger text-success');
	}
}

function totalBohlam(){
	var harga = parseFloat(jQuery('#bohlam_harga').val());
	var voltase = parseFloat(jQuery('#bohlam_voltase').val());
	var daya_power = parseFloat(jQuery('#bohlam_daya_power').val());
	var daya_power_dekat = parseFloat(jQuery('#bohlam_daya_power_dekat').val());
	var daya_power_jauh = parseFloat(jQuery('#bohlam_daya_power_jauh').val());
	var pancaran_cahaya = parseFloat(jQuery('#bohlam_pancaran_cahaya').val());
	var total = harga + voltase;
	console.log(total);
	if(jQuery('#jenis-bohlam-option').val() == "Lampu Depan"){
		if(jQuery('#ukuran-bohlam-option').val() == "H4"){
			total += daya_power_dekat + daya_power_jauh;
		}
		else{
			total += daya_power;
		}
		total += pancaran_cahaya;
	}
	else{
		total += daya_power;
	}
	if(isNaN(total)){
		jQuery('#total').html("Total is not valid!").addClass('text-danger').removeClass('text-success text-warning');
	}
	else if(total == 100){
		jQuery('#total').html("Total is " + total).addClass('text-success').removeClass('text-danger text-warning');
	}
	else{
		jQuery('#total').html("Total is " + total).addClass('text-warning').removeClass('text-danger text-success');
	}
}