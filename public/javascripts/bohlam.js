function extraByJenisBohlam(obj){
	var option = obj.value;
	if(option == "Lampu Depan"){
		jQuery('#ukuran-bohlam').attr('style', 'display:table-row');
		jQuery('#pancaran-cahaya').attr('style', 'display:table-row');
	}
	else{
		jQuery('#daya-power').attr('style', 'display:table-row');
		jQuery('#ukuran-bohlam').attr('style', 'display:none');
		jQuery('#pancaran-cahaya').attr('style', 'display:none');
		jQuery('#daya-power-dekat').attr('style', 'display:none');
		jQuery('#daya-power-jauh').attr('style', 'display:none');
		jQuery('#ukuran-bohlam-option').val("");
		jQuery('#pancaran_cahaya').val("");
		jQuery('#daya_power_dekat').val("");
		jQuery('#daya_power_jauh').val("");
	}

	if(jQuery('#ukuran-bohlam-option').prop('selectedIndex') == 2 && jQuery('#ukuran-bohlam').css('display') != 'none'){
		jQuery('#daya-power').attr('style', 'display:none');
		jQuery('#daya_power').val("");
		jQuery('#daya-power-dekat').attr('style', 'display:table-row');
		jQuery('#daya-power-jauh').attr('style', 'display:table-row');
	}
	else{
		jQuery('#daya-power').attr('style', 'display:table-row');
		jQuery('#daya-power-dekat').attr('style', 'display:none');
		jQuery('#daya-power-jauh').attr('style', 'display:none');
		jQuery('#daya_power_dekat').val("");
		jQuery('#daya_power_jauh').val("");
	}
}

function extraByUkuranBohlam(obj){
	var option = obj.value;
	if(option == "H4"){
		jQuery('#daya-power').attr('style', 'display:none');
		jQuery('#daya_power').val("");
		jQuery('#daya-power-dekat').attr('style', 'display:table-row');
		jQuery('#daya-power-jauh').attr('style', 'display:table-row');
	}
	else{
		jQuery('#daya-power').attr('style', 'display:table-row');
		jQuery('#daya-power-dekat').attr('style', 'display:none');
		jQuery('#daya-power-jauh').attr('style', 'display:none');
		jQuery('#daya_power_dekat').val("");
		jQuery('#daya_power_jauh').val("");
	}
}

jQuery('#save_data').click(function(){
	jQuery('#save_form').submit();
});

jQuery(window).load(function (){
	extraByJenisBohlam(document.getElementById("jenis-bohlam-option"));
	extraByUkuranBohlam(document.getElementById("ukuran-bohlam-option"));
});