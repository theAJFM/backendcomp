jQuery(window).load(function (){
	jQuery('.content').load('/input_oli', function() {
	});
});

function pilihJenisProduk(obj){
	var option = obj.value;
	var action;
	var page;
	if(option == 1){
		page = 'input_oli';
		action = 'add_oli';
	}
	else if(option == 2){
		page = 'input_cat';
		action = 'add_cat';
	}
	else if(option == 3){
		page = 'input_aki';
		action = 'add_aki';
	}
	else if(option == 4){
		page = 'input_minyak_rem';
		action = 'add_minyak_rem';
	}
	else if(option == 5){
		page = 'input_kaca_film';
		action = 'add_kaca_film';
	}
	else if(option == 6){
		page = 'input_cat_vernis';
		action = 'add_cat_vernis';
	}
	else if(option == 7){
		page = 'input_bohlam';
		action = 'add_bohlam';
	}

	jQuery('.content').load('/'+page, function() {
		jQuery('select#jenis_produk').val(option);
		jQuery('#save_form').attr('action', action);
	});
}