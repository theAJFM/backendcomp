var PouchDB = require('pouchdb');
var oli = new PouchDB(__dirname + '/../oli');
var aki = new PouchDB(__dirname + '/../aki');
var bohlam = new PouchDB(__dirname + '/../bohlam');
var cat = new PouchDB(__dirname + '/../cat');
var catVernis = new PouchDB(__dirname + '/../cat_vernis');
var kacaFilm = new PouchDB(__dirname + '/../kaca_film');
var minyakRem = new PouchDB(__dirname + '/../minyak_rem');
var utility = new PouchDB(__dirname + '/../utility');

var admin = {_id:"admin", password:"test"};

var batch = function(response){
	utility.bulkDocs([
		{
			bahan_bakar: [
			'Bensin & Diesel',
			'Bensin',
			'Diesel'
			],
			api: {
			'SF' : [0, 1988],
			'SG' : [0, 1993],
			'SL' : [0, 1996],
			'SJ' : [0, 2001],
			'SM' : [2004, 2015],
			'SN' : [2004, 2015],
			'CF' : [1994, Number.MAX_VALUE],
			'CH-4' : [1998, Number.MAX_VALUE],
			'CI-4' : [2002, Number.MAX_VALUE],
			'CJ-4' : [2006, Number.MAX_VALUE]
			},
			viskositas: {
				"10W-30" : 1,
				"10W-40" : 1,
				"10W-50" : 1,
				"15W-30" : 1,
				"15W-40" : 0.6,
				"15W-50" : 1,
				"20W-50" : 0.3
			},
			ketahanan: {
				"Mineral" : 0.3,
				"Semi Synthetic" : 0.6,
				"Full Synthetic" : 1
			},
			_id: 'oli_fields'
		},
		{
			warna_cat : [
			'Red',
			'Green',
			'Pink',
			'Blue',
			'Orange',
			'Maroon',
			'White',
			'Violet',
			'Yellow',
			'Silver Fine',
			'Silver Bright',
			'Red Brown',
			'Pearl Violet',
			'Deep Black',
			'Pearl Yellow',
			'Pearl Gold',
			'Dark Red',
			'Pearl Red',
			'Helio Blue',
			'Pearl Green'
			],
			daya_kilap: {'Tinggi': 1, 'Rendah': 0.5},
			daya_tahan_cat: {'Tahan Lama': 1, 'Cepat Pudar': 0.5},
			jenis_cat: [
			'Poly-Urethane',
			'Nitro-Cellulose'
			],
			_id: 'cat_fields'
		},

		{
			tipe_aki: ['Basah', 'Kering'], 
			silinder_mesin: [
			'1000',
			'1300',
			'1500',
			'1800',
			'2000',
			'2400',
			'3500'
			],
			_id: 'aki_fields'
		},
		{
			warna: [
			'Merah',
			'Putih',
			'Biru',
			'Hitam',
			],
			DOT:['3', '4', '5', '5.1'],
			_id:'minyak_rem_fields'
		},
		{
			basis_warna:[
			'Hitam Kecoklatan',
			'Kebiruan',
			'Silver Reflective'
			],
			_id:'kaca_film_fields'
		},
		{
			daya_tahan_kilap:{'9:1':9, '4:1':4, '2:1':2},
			_id:'cat_vernis_fields'
		},
		{
			tipe_bohlam: ['LID', 'LED'],
			jenis_bohlam: [
				'Lampu Depan',
				'Lampu Stop',
				'Lampu Sein & Mundur'
			],
			ukuran_bohlam: [
				'H1',
				'H2',
				'H4',
				'HB3',
				'HB4',
				'H8',
				'H11'
			],
			_id:'bohlam_fields'
		}
	]);
};

var log = function(err){
	console.log(err);
};

utility
.post(admin)
.then(batch())
.catch(log);
