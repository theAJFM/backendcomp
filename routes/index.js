var express = require('express');
var debug = require('debug');
var error = debug('app:error');
var bodyParser = require('body-parser');
var validation = require('./../helpers/validation');
var calculation = require('./../helpers/calculation');
var fileUpload = require('./../helpers/fileUpload');
var router = express.Router();

var log = debug('app:log');
log.log = console.log.bind(console);

var PouchDB = require('pouchdb');
PouchDB.debug.enable('*');
var oli = new PouchDB(__dirname + '/../oli');
var aki = new PouchDB(__dirname + '/../aki');
var bohlam = new PouchDB(__dirname + '/../bohlam');
var cat = new PouchDB(__dirname + '/../cat');
var catVernis = new PouchDB(__dirname + '/../cat_vernis');
var kacaFilm = new PouchDB(__dirname + '/../kaca_film');
var minyakRem = new PouchDB(__dirname + '/../minyak_rem');
var utility = new PouchDB(__dirname + '/../utility');
EDITCONST = {
		"oli" : {link: "input_partials/input_oli.ejs", field: "oli_fields", object: oli, errors: function(body){return validation.oliErrors(body);}},
		"cat" : {link: "input_partials/input_cat.ejs", field: "cat_fields", object: cat, errors: function(body){return validation.catErrors(body);}},
		"aki" : {link: "input_partials/input_aki.ejs", field: "aki_fields", object: aki, errors: function(body){return validation.akiErrors(body);}},
		"minyak_rem" : {link: "input_partials/input_minyak_rem.ejs", field: "minyak_rem_fields", object: minyakRem, errors: function(body){return validation.minyakRemErrors(body);}},
		"kaca_film" : {link: "input_partials/input_kaca_film.ejs", field: "kaca_film_fields", object: kacaFilm, errors: function(body){return validation.kacaFilmErrors(body);}},
		"cat_vernis" : {link: "input_partials/input_cat_vernis.ejs", field: "cat_vernis_fields", object: catVernis, errors: function(body){return validation.catVernisErrors(body);}},
		"bohlam" : {link: "input_partials/input_bohlam.ejs", field: "bohlam_fields", object: bohlam, errors: function(body){return validation.bohlamErrors(body);}}
};

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index.ejs', {user:req.session.username});
});

router.get('/logout', function(req, res, next){
	req.session.destroy();
	res.redirect('/');
});

router.get('/help', function(req, res, next){
	res.render('help.ejs', {user:req.session.username});
});

router.get('/signin', function(req, res, next){
	res.render('signin.ejs', {layout:false});
});

router.get('/profile', function(req, res, next){
	res.render('profile.ejs', {user:req.session.username});
});

router.get('/service_price_list', function(req, res, next){
	res.render('service_price_list.ejs', {user:req.session.username});
});

router.get('/bengkel_price_list', function(req, res, next){
	res.render('bengkel_price_list.ejs', {user:req.session.username});
});

router.get('/input_data', function(req, res, next){
	res.render('input_data.ejs', {layout:false, user:req.session.username});
});

router.get('/determine_best_product', function(req, res, next){
	res.render('determine_best_product.ejs', {layout:false, user:req.session.username});
});

router.get('/input_oli', function(req, res, next){
	utility.get('oli_fields', function(err, doc){
		if(err)console.log(err);
		res.render('input_partials/input_oli.ejs', {layout:false, doc:{api:[], warna_cat:[], warna:[], silinder_mesin:[], basis_warna:[]}, fields:doc});
	});
});

router.get('/input_aki', function(req, res, next){
	utility.get('aki_fields', function(err, doc){
		if(err)console.log(err);
		res.render('input_partials/input_aki.ejs', {layout:false, doc:{api:[], warna_cat:[], warna:[], silinder_mesin:[], basis_warna:[]}, fields:doc});
	});
});

router.get('/input_cat', function(req, res, next){
	utility.get('cat_fields', function(err, doc){
		if(err)console.log(err);
		res.render('input_partials/input_cat.ejs', {layout:false, doc:{api:[], warna_cat:[], warna:[], silinder_mesin:[], basis_warna:[]}, fields:doc});
	});
});

router.get('/input_bohlam', function(req, res, next){
	utility.get('bohlam_fields', function(err, doc){
		if(err)console.log(err);
		res.render('input_partials/input_bohlam.ejs', {layout:false, doc:{api:[], warna_cat:[], warna:[], silinder_mesin:[], basis_warna:[]}, fields:doc});
	});
});

router.get('/input_minyak_rem', function(req, res, next){
	utility.get('minyak_rem_fields', function(err, doc){
		if(err)console.log(err);
		res.render('input_partials/input_minyak_rem.ejs', {layout:false, doc:{api:[], warna_cat:[], warna:[], silinder_mesin:[], basis_warna:[]}, fields:doc});
	});
});

router.get('/input_cat_vernis', function(req, res, next){
	utility.get('cat_vernis_fields', function(err, doc){
		if(err)console.log(err);
		res.render('input_partials/input_cat_vernis.ejs', {layout:false, doc:{api:[], warna_cat:[], warna:[], silinder_mesin:[], basis_warna:[]}, fields:doc});
	});
});

router.get('/input_kaca_film', function(req, res, next){
	utility.get('kaca_film_fields', function(err, doc){
		if(err)console.log(err);
		res.render('input_partials/input_kaca_film.ejs', {layout:false, doc:{api:[], warna_cat:[], warna:[], silinder_mesin:[], basis_warna:[]}, fields:doc});
	});
});

router.get('/output_oli', function(req, res, next){
	utility.get('oli_fields', function(err, doc){
		if(err)console.log(err);
		res.render('output_partials/output_oli.ejs', {layout:false, fields:doc});
	});
});

router.get('/output_aki', function(req, res, next){
	utility.get('aki_fields', function(err, doc){
		if(err)console.log(err);
		res.render('output_partials/output_aki.ejs', {layout:false, fields:doc});
	});
});

router.get('/output_cat', function(req, res, next){
	utility.get('cat_fields', function(err, doc){
		if(err)console.log(err);
		res.render('output_partials/output_cat.ejs', {layout:false, fields:doc});
	});
});

router.get('/output_bohlam', function(req, res, next){
	utility.get('bohlam_fields', function(err, doc){
		if(err)console.log(err);
		res.render('output_partials/output_bohlam.ejs', {layout:false, fields:doc});
	});
});

router.get('/output_minyak_rem', function(req, res, next){
	utility.get('minyak_rem_fields', function(err, doc){
		if(err)console.log(err);
		res.render('output_partials/output_minyak_rem.ejs', {layout:false, fields:doc});
	});
});

router.get('/output_cat_vernis', function(req, res, next){
	res.render('output_partials/output_cat_vernis.ejs', {layout:false});
});

router.get('/output_kaca_film', function(req, res, next){
	res.render('output_partials/output_kaca_film.ejs', {layout:false});
});

router.get('/oli_list', function(req, res, next){
	oli.allDocs({
		include_docs: true,
		attachments: true
	}, function(err, response) {
		if (err) { return console.log(err); }
		response.rows.sort(calculation.sorted);
		res.render('product_partials/oli_list.ejs', {layout:false, user:req.session.username, data:response});
	});
});

router.get('/cat_list', function(req, res, next){
	cat.allDocs({
		include_docs: true,
		attachments: true
	}, function(err, response) {
		if (err) { return console.log(err); }
		response.rows.sort(calculation.sorted);
		res.render('product_partials/cat_list.ejs', {layout:false, user:req.session.username, data:response});
	});
});

router.get('/aki_list', function(req, res, next){
	aki.allDocs({
		include_docs: true,
		attachments: true
	}, function(err, response) {
		if (err) { return console.log(err); }
		response.rows.sort(calculation.sorted);
		res.render('product_partials/aki_list.ejs', {layout:false, user:req.session.username, data:response});
	});
});

router.get('/minyak_rem_list', function(req, res, next){
	minyakRem.allDocs({
		include_docs: true,
		attachments: true
	}, function(err, response) {
		if (err) { return console.log(err); }
		response.rows.sort(calculation.sorted);
		res.render('product_partials/minyak_rem_list.ejs', {layout:false, user:req.session.username, data:response});
	});
});

router.get('/kaca_film_list', function(req, res, next){
	kacaFilm.allDocs({
		include_docs: true,
		attachments: true
	}, function(err, response) {
		if (err) { return console.log(err); }
		response.rows.sort(calculation.sorted);
		res.render('product_partials/kaca_film_list.ejs', {layout:false, user:req.session.username, data:response});
	});
});

router.get('/cat_vernis_list', function(req, res, next){
	catVernis.allDocs({
		include_docs: true,
		attachments: true
	}, function(err, response) {
		if (err) { return console.log(err); }
		response.rows.sort(calculation.sorted);
		res.render('product_partials/cat_vernis_list.ejs', {layout:false, user:req.session.username, data:response});
	});
});

router.get('/bohlam_list', function(req, res, next){
	bohlam.allDocs({
		include_docs: true,
		attachments: true
	}, function(err, response) {
		if (err) { return console.log(err); }
		response.rows.sort(calculation.sorted);
		res.render('product_partials/bohlam_list.ejs', {layout:false, user:req.session.username, data:response});
	});
});

router.get('/list_products', function(req, res, next){
	res.render('list_products.ejs', {layout:false, user:req.session.username});
});

router.post('/login', function(req, res, next){
	var errors = [];
	if(req.body.username == "admin"){
		utility.get('admin', function(err, doc){
			if(doc.password == req.body.password){
				req.session.username = "admin";
				res.redirect('/');
			}
			else{
				errors.push("Wrong username/password combination");
				res.render('signin.ejs', {layout:false, 'errors':errors});
			}
		});
	}
	else{
		errors.push("Wrong username/password combination");
		res.render('signin.ejs', {layout:false, 'errors':errors});
	}
});

router.get('/change_password', function(req, res, next){
	res.render('change_password.ejs', {user:req.session.username});
});

router.post('/update_password', function(req, res, next){
	var errors = [];
	var oldPassword = req.body.old_password;
	var newPassword = req.body.new_password;
	var confirmPassword = req.body.confirm_password;

	utility.get('admin', function(err, doc){
		if(doc.password != oldPassword){
			errors.push("Incorrect old password");
			res.render('change_password.ejs', {'errors':errors});
		}
		else if(newPassword === ''){
			errors.push("New password can't be empty");
			res.render('change_password.ejs', {'errors':errors});
		}
		else{
			if(newPassword == confirmPassword){
				doc.password = newPassword;
				utility.put(doc).then(res.redirect('/')).catch(function(err){console.log(err);});
			}
			errors.push("New password and confirm password do not match!");
			res.render('change_password.ejs', {'errors':errors});
		}
	});
});

router.post('/add_oli', function(req, res, next){
	fileUpload.upload(req, res, function(errFile, fileInfo){
		if(errFile){
			return console.log(errFile);
		}
		var errors = validation.oliErrors(req.body);

		req.body._id = req.body.kode_produk;
		if(typeof fileInfo.file  !== "undefined"){
			req.body.photo = "/uploads/" + fileInfo.file.filename;
		}
		if(errors.length > 0){
			res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
		}
		else{
			oli.post(req.body)
			.then(function(response){
				return res.redirect('/');
			})
			.catch(function(err){
				console.log(err);
				var errors = ["Kode produk tidak bisa berduplikat"];
				return res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
			});
		}
	});
});

router.post('/add_cat', function(req, res, next){
	fileUpload.upload(req, res, function(errFile, fileInfo){
		if(errFile){
			return console.log(errFile);
		}
		var errors = validation.catErrors(req.body);

		req.body._id = req.body.kode_produk;
		if(typeof fileInfo.file  !== "undefined"){
			req.body.photo = "/uploads/" + fileInfo.file.filename;
		}
		if(errors.length > 0){
			res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
		}
		else{
			cat.post(req.body)
			.then(function(response){
				return res.redirect('/');
			})
			.catch(function(err){
				console.log(err);
				var errors = ["Kode produk tidak bisa berduplikat"];
				return res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
			});
		}
	});
});

router.post('/add_aki', function(req, res, next){
	fileUpload.upload(req, res, function(errFile, fileInfo){
		if(errFile){
			return console.log(errFile);
		}
		var errors = validation.akiErrors(req.body);

		req.body._id = req.body.kode_produk;
		if(typeof fileInfo.file  !== "undefined"){
			req.body.photo = "/uploads/" + fileInfo.file.filename;
		}
		if(errors.length > 0){
			res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
		}
		else{
			aki.post(req.body)
			.then(function(response){
				return res.redirect('/');
			})
			.catch(function(err){
				console.log(err);
				var errors = ["Kode produk tidak bisa berduplikat"];
				return res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
			});
		}
	});
});

router.post('/add_minyak_rem', function(req, res, next){
	fileUpload.upload(req, res, function(errFile, fileInfo){
		if(errFile){
			return console.log(errFile);
		}
		var errors = validation.minyakRemErrors(req.body);

		req.body._id = req.body.kode_produk;
		if(typeof fileInfo.file  !== "undefined"){
			req.body.photo = "/uploads/" + fileInfo.file.filename;
		}
		if(errors.length > 0){
			res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
		}
		else{
			minyakRem.post(req.body)
			.then(function(response){
				return res.redirect('/');
			})
			.catch(function(err){
				console.log(err);
				var errors = ["Kode produk tidak bisa berduplikat"];
				return res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
			});
		}
	});
});

router.post('/add_kaca_film', function(req, res, next){
	fileUpload.upload(req, res, function(errFile, fileInfo){
		if(errFile){
			return console.log(errFile);
		}
		var errors = validation.kacaFilmErrors(req.body);

		req.body._id = req.body.kode_produk;
		if(typeof fileInfo.file  !== "undefined"){
			req.body.photo = "/uploads/" + fileInfo.file.filename;
		}
		if(errors.length > 0){
			res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
		}
		else{
			kacaFilm.post(req.body)
			.then(function(response){
				return res.redirect('/');
			})
			.catch(function(err){
				console.log(err);
				var errors = ["Kode produk tidak bisa berduplikat"];
				return res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
			});
		}
	});
});

router.post('/add_cat_vernis', function(req, res, next){
	fileUpload.upload(req, res, function(errFile, fileInfo){
		if(errFile){
			return console.log(errFile);
		}
		var errors = validation.catVernisErrors(req.body);

		req.body._id = req.body.kode_produk;
		if(typeof fileInfo.file  !== "undefined"){
			req.body.photo = "/uploads/" + fileInfo.file.filename;
		}
		if(errors.length > 0){
			res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
		}
		else{
			catVernis.post(req.body)
			.then(function(response){
				return res.redirect('/');
			})
			.catch(function(err){
				console.log(err);
				var errors = ["Kode produk tidak bisa berduplikat"];
				return res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
			});
		}
	});
});

router.post('/add_bohlam', function(req, res, next){
	fileUpload.upload(req, res, function(errFile, fileInfo){
		if(errFile){
			return console.log(errFile);
		}
		var errors = validation.bohlamErrors(req.body);

		req.body._id = req.body.kode_produk;
		if(typeof fileInfo.file  !== "undefined"){
			req.body.photo = "/uploads/" + fileInfo.file.filename;
		}
		if(errors.length > 0){
			res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
		}
		else{
			bohlam.post(req.body)
			.then(function(response){
				return res.redirect('/');
			})
			.catch(function(err){
				console.log(err);
				var errors = ["Kode produk tidak bisa berduplikat"];
				return res.render('input_data.ejs', {layout:false, user:req.session.username, 'errors': errors});
			});
		}
	});
});

router.post('/find_oli', function(req, res, next){
	var errors = validation.oliKriteriaErrors(req.body);
	var min_harga = Number.MAX_VALUE;
	var max_viskositas = 0;
	var max_ketahanan = 0;
	if(errors.length > 0){
		res.send(errors);
	}
	else{
		utility.get('oli_fields', function(err, fields){
			var list = [];
			for(var key in fields.api){
				if(req.body.tahun_produksi <= fields.api[key][1] && req.body.tahun_produksi >= fields.api[key][0]){
					list.push(key);
				}
			}
			oli.query(function(doc, emit){
				var approved = false;
				for(var i=0;i<list.length;i++){
					if(doc.api.indexOf(list[i]) != -1 && doc.bahan_bakar == req.body.bahan_bakar){
						approved = true;
					}
				}
				if(approved){
					min_harga = Math.min(min_harga, doc.harga);
					max_viskositas = Math.max(max_viskositas, fields.viskositas[doc.viskositas]);
					max_ketahanan = Math.max(max_ketahanan, fields.ketahanan[doc.ketahanan]);
					emit(doc);
				}
			}, {include_docs:true})
			.then(function(result){
				var bestMatches = calculation.calculateOli(min_harga, max_viskositas, max_ketahanan, req.body.kriteria_harga, req.body.kriteria_viskositas, req.body.kriteria_ketahanan, result, fields);
				res.render('product_partials/oli_list.ejs', {layout:false, user:req.session.username, data:bestMatches, preference:true});
			})
			.catch(function(err){
				console.log(err);
			});
		});
	}
});

router.post('/find_cat', function(req, res, next){
	var errors = validation.catKriteriaErrors(req.body);
	var min_harga = Number.MAX_VALUE;
	var min_daya_kering = Number.MAX_VALUE;
	var max_daya_sebar = 0;
	if(errors.length > 0){
		res.send(errors);
	}
	else{
		utility.get('cat_fields', function(err, fields){
			cat.query(function(doc, emit){
				var approved = false;
				for(var i=0;i<req.body.warna_cat.length;i++){
					if(doc.warna_cat.indexOf(req.body.warna_cat[i]) != -1){
						approved = true;
						break;
					}
				}
				if(approved){
					min_harga = Math.min(min_harga, doc.harga);
					min_daya_kering = Math.min(min_daya_kering, doc.daya_kering);
					max_daya_sebar = Math.max(max_daya_sebar, doc.daya_sebar);
					emit(doc);
				}
			}, {include_docs:true})
			.then(function (result) {
				var bestMatches = calculation.calculateCat(min_harga, min_daya_kering, max_daya_sebar, req.body.kriteria_harga, req.body.kriteria_daya_kering, req.body.kriteria_daya_sebar, req.body.kriteria_daya_tahan, req.body.kriteria_daya_kilap, result, fields);
				res.render('product_partials/cat_list.ejs', {layout:false, user:req.session.username, data:bestMatches, preference:true});
			})
			.catch(function (err) {
				console.log(err);
			});
		});
	}
});

router.post('/find_aki', function(req, res, next){
	var errors = validation.akiKriteriaErrors(req.body);
	var min_harga = Number.MAX_VALUE;
	var max_kapasitas = 0;
	var max_tegangan = 0;
	var max_CCA = 0;
	if(errors.length > 0){
		res.send(errors);
	}
	else{
		aki.query(function(doc, emit){
			var approved = false;
			for(var i=0;i<req.body.silinder_mesin.length;i++){
				if(doc.silinder_mesin.indexOf(req.body.silinder_mesin[i]) != -1){
					approved = true;
					break;
				}
			}
			if(approved && doc.tipe_aki == req.body.tipe_aki){
				min_harga = Math.min(min_harga, doc.harga);
				max_kapasitas = Math.max(max_kapasitas, doc.kapasitas);
				max_tegangan = Math.max(max_tegangan, doc.tegangan);
				max_CCA = Math.max(max_CCA, doc.cca);
				emit(doc);
			}
		}, {include_docs:true})
		.then(function(result){
			var bestMatches = calculation.calculateAki(min_harga, max_kapasitas, max_tegangan, max_CCA, req.body.kriteria_harga, req.body.kriteria_kapasitas, req.body.kriteria_tegangan, req.body.kriteria_CCA, result);
			res.render('product_partials/aki_list.ejs', {layout:false, user:req.session.username, data:bestMatches, preference:true});
		})
		.catch(function(err){
			console.log(err);
		});
	}
});

router.post('/find_minyak_rem', function(req, res, next){
	var errors = validation.minyakRemKriteriaErrors(req.body);
	var min_harga = Number.MAX_VALUE;
	var max_boiling_point = 0;
	var max_wet_boiling_point = 0;
	if(errors.length > 0){
		res.send(errors);
	}
	else{
		minyakRem.query(function(doc, emit){
			var approved = false;
			for(var i=0;i<req.body.DOT.length;i++){
				if(doc.DOT == req.body.DOT[i]){
					min_harga = Math.min(min_harga, doc.harga);
					max_boiling_point = Math.max(max_boiling_point, doc.boiling_point);
					max_wet_boiling_point = Math.max(max_wet_boiling_point, doc.wet_boiling_point);
					emit(doc);
					break;
				}
			}
		}, {include_docs:true})
		.then(function(result){
			var bestMatches = calculation.calculateMinyakRem(min_harga, max_boiling_point, max_wet_boiling_point, req.body.kriteria_harga, req.body.kriteria_boiling_point, req.body.kriteria_wet_boiling_point, result);
			res.render('product_partials/minyak_rem_list.ejs', {layout:false, user:req.session.username, data:bestMatches, preference:true});
		})
		.catch(function(err){
			console.log(err);
		});
	}
});

router.post('/find_kaca_film', function(req, res, next){
	var errors = validation.kacaFilmKriteriaErrors(req.body);
	var min_harga = Number.MAX_VALUE;
	var max_VLT = 0;
	var max_UVR = 0;
	var max_IRR = 0;
	var max_TSER = 0;
	if(errors.length > 0){
		res.send(errors);
	}
	else{
		kacaFilm.query(function(doc, emit){
			for(var i=0;i<req.body.kegelapan.length;i++){
				if(req.body.kegelapan[i] == "10-20"){
					if(doc.kegelapan >= 10 && doc.kegelapan <= 20){
						min_harga = Math.min(min_harga, doc.harga);
						max_VLT = Math.max(max_VLT, doc.VLT);
						max_UVR = Math.max(max_UVR, doc.UVR);
						max_IRR = Math.max(max_IRR, doc.IRR);
						max_TSER = Math.max(max_TSER, doc.TSER);
						emit(doc);
						break;
					}
				}
				else if(req.body.kegelapan[i] == "21-40"){
					if(doc.kegelapan > 20 && doc.kegelapan <= 40){
						min_harga = Math.min(min_harga, doc.harga);
						max_VLT = Math.max(max_VLT, doc.VLT);
						max_UVR = Math.max(max_UVR, doc.UVR);
						max_IRR = Math.max(max_IRR, doc.IRR);
						max_TSER = Math.max(max_TSER, doc.TSER);
						emit(doc);
						break;
					}
				}
				else if(req.body.kegelapan[i] == "41-60"){
					if(doc.kegelapan > 40 && doc.kegelapan <= 60){
						min_harga = Math.min(min_harga, doc.harga);
						max_VLT = Math.max(max_VLT, doc.VLT);
						max_UVR = Math.max(max_UVR, doc.UVR);
						max_IRR = Math.max(max_IRR, doc.IRR);
						max_TSER = Math.max(max_TSER, doc.TSER);
						emit(doc);
						break;
					}
				}
				else{
					if(doc.kegelapan > 60 && doc.kegelapan <= 80){
						min_harga = Math.min(min_harga, doc.harga);
						max_VLT = Math.max(max_VLT, doc.VLT);
						max_UVR = Math.max(max_UVR, doc.UVR);
						max_IRR = Math.max(max_IRR, doc.IRR);
						max_TSER = Math.max(max_TSER, doc.TSER);
						emit(doc);
						break;
					}
				}
			}
		}, {include_docs:true})
.then(function(result){
	var bestMatches = calculation.calculateKacaFilm(min_harga, max_VLT, max_UVR, max_IRR, max_TSER, req.body.kriteria_harga, req.body.kriteria_VLT, req.body.kriteria_UVR, req.body.kriteria_IRR, req.body.kriteria_TSER, result);
	res.render('product_partials/kaca_film_list.ejs', {layout:false, user:req.session.username, data:bestMatches, preference:true});
})
.catch(function(err){
	console.log(err);
});
}
});

router.post('/find_cat_vernis', function(req, res, next){
	var errors = validation.catVernisKriteriaErrors(req.body);
	var min_harga = Number.MAX_VALUE;
	var max_daya_tahan_kilap = 0;
	var min_daya_pengeringan = Number.MAX_VALUE;
	if(errors.length > 0){
		res.send(errors);
	}
	else{
		utility.get('cat_vernis_fields', function(err, fields){
			catVernis.query(function(doc, emit){
				min_harga = Math.min(min_harga, doc.harga);
				max_daya_tahan_kilap = Math.max(max_daya_tahan_kilap, fields.daya_tahan_kilap[doc.daya_tahan_kilap]);
				min_daya_pengeringan = Math.min(min_daya_pengeringan, doc.daya_pengeringan);
				emit(doc);
			}, {include_docs:true})
			.then(function (result) {
				console.log(min_harga);
				console.log(max_daya_tahan_kilap);
				console.log(min_daya_pengeringan);
				var bestMatches = calculation.calculateCatVernis(min_harga, max_daya_tahan_kilap, min_daya_pengeringan, req.body.kriteria_harga, req.body.kriteria_daya_tahan_kilap, req.body.kriteria_pengeringan, result, fields);
				res.render('product_partials/cat_vernis_list.ejs', {layout:false, user:req.session.username, data:bestMatches, preference:true});
			})
			.catch(function (err) {
				console.log(err);
			});
		});
	}
});

router.post('/find_bohlam', function(req, res, next){
	var errors = validation.bohlamKriteriaErrors(req.body);
	var min_harga = Number.MAX_VALUE;
	var max_voltase = 0;
	var max_daya_power = 0;
	var max_daya_power_jauh = 0;
	var max_daya_power_dekat = 0;
	var max_pancaran_cahaya = 0;
	var jenis_bohlam = req.body.jenis_bohlam;
	var ukuran_bohlam = req.body.ukuran_bohlam;
	if(errors.length > 0){
		res.send(errors);
	}
	else{
		bohlam.query(function(doc, emit){
			if(doc.jenis_bohlam == jenis_bohlam){
				if(doc.jenis_bohlam == "Lampu Depan"){
					if(doc.ukuran_bohlam == ukuran_bohlam){
						min_harga = Math.min(min_harga, doc.harga);
						max_pancaran_cahaya = Math.max(max_pancaran_cahaya, doc.pancaran_cahaya);
						max_voltase = Math.max(max_voltase, doc.voltase);
						if(doc.ukuran_bohlam == "H4"){
							max_daya_power_jauh = Math.max(max_daya_power_jauh, doc.daya_power_jauh);
							max_daya_power_dekat = Math.max(max_daya_power_dekat, doc.daya_power_dekat);
						}
						else{
							max_daya_power = Math.max(max_daya_power, doc.daya_power);
						}
						emit(doc);
					}
				}
				else{
					min_harga = Math.min(min_harga, doc.harga);
					max_daya_power = Math.max(max_daya_power, doc.daya_power);
					max_voltase = Math.max(max_voltase, doc.voltase);
					emit(doc);
				}
			}
		},{include_docs:true})
		.then(function(result){
			var bestMatches = calculation.calculateBohlam(min_harga, max_voltase, max_daya_power, max_daya_power_jauh, max_daya_power_dekat, max_pancaran_cahaya, req.body.kriteria_harga, req.body.kriteria_voltase, req.body.kriteria_daya_power, req.body.kriteria_daya_power_jauh, req.body.kriteria_daya_power_dekat, req.body.kriteria_pancaran_cahaya, result);
			console.log(bestMatches);
			res.render('product_partials/bohlam_list.ejs', {layout:false, user:req.session.username, data:bestMatches, preference:true});
		})
		.catch(function(err){
			console.log(err);
		});
	}
});

router.get('/edit_item/:itemType/:itemId', function(req, res, next){
	
	var itemId = req.params.itemId;
	var itemType = EDITCONST[req.params.itemType];
	utility.get(itemType.field, function(err, fields){
		if(err) console.log(err);
		itemType.object.get(itemId, function(err, doc){
		if(err) { 
			var errNotFound = new Error('Not Found');
  			errNotFound.status = 404;
  			return next(errNotFound);
		}
			res.render(itemType.link, {layout: "edit_layout", doc:doc, fields:fields, edit:true, user:req.session.username, itemId:itemId, itemType:req.params.itemType});
		});
	});
});

router.post('/update_item/:itemType/:itemId', function(req, res, next){
	var itemId = req.params.itemId;
	var itemType = EDITCONST[req.params.itemType];
	itemType.object.get(itemId, function(err, doc){
		if(err) { return console.log(err); }
		fileUpload.upload(req, res, function(errFile, fileInfo){
			if(errFile){
				return console.log(errFile);
			}
			var errors = itemType.errors(req.body);
			if(errors.length > 0){
				res.render(itemType.link, {layout: "edit_layout", doc:doc, edit:true, user:req.session.username, itemId:itemId, 'errors': errors});
			}
			else{
				req.body._id = itemId;
				req.body._rev = doc._rev;
				if(typeof fileInfo.file  !== "undefined"){
					req.body.photo = "/uploads/" + fileInfo.file.filename;
				}
				else{
					req.body.photo = doc.photo;
				}
				itemType.object.put(req.body, function(err, response) {
				    if (err) { return console.log(err); }
				    res.redirect('/');
				});
			}
		});
	});
});

router.get('/delete_item/:itemType/:itemId', function(req, res, next){
	var itemId = req.params.itemId;
	var itemType = EDITCONST[req.params.itemType];
	itemType.object.get(itemId, function(err, doc){
		if(err) return console.log(err);
		return itemType.object.remove(doc).then(res.redirect('/')).catch(function(err){console.log(err);});
	});
});

router.get('/edit_fields/:itemType', function(req, res, next){
	var itemType = EDITCONST[req.params.itemType];
	var itemLink = "field_input_partials/" + itemType.field + ".ejs";
	utility.get(itemType.field, function(err, doc){
		if(err) console.log(err);
		res.render(itemLink, {doc:{api:[], warna_cat:[], warna:[], silinder_mesin:[], basis_warna:[]},  user:req.session.username, fields:doc});
	});
});

router.get('/delete_fields/:itemType/:fieldType/:value', function(req, res, next){
	var itemType = EDITCONST[req.params.itemType];
	var fieldType = req.params.fieldType;
	var value = req.params.value;
	utility.get(itemType.field, function(err, doc){
		if(err) console.log(err);
		switch(req.params.itemType){
			case "oli":
				if(fieldType == "viskositas" || fieldType == "ketahanan" || fieldType == "api"){
					delete doc[fieldType][value];
				}
				else{
					doc[fieldType].splice(value, 1);
				}
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "cat":
				if(fieldType == "jenis_cat" || fieldType == "warna_cat"){
					doc[fieldType].splice(value, 1);
				}
				else{
					delete doc[fieldType][value];
				}
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "aki":
				doc[fieldType].splice(value, 1);
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "minyak_rem":
				doc[fieldType].splice(value, 1);
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "cat_vernis":
				delete doc[fieldType][value];
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "kaca_film":
				doc[fieldType].splice(value, 1);
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "bohlam":
				doc[fieldType].splice(value, 1);
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			default:
				break;
		}
	});
});

router.post('/add_fields/:itemType/:fieldType', function(req, res, next){
	var itemType = EDITCONST[req.params.itemType];
	var fieldType = req.params.fieldType;
	var value = req.body;
	utility.get(itemType.field, function(err, doc){
		if(err) console.log(err);
		switch(req.params.itemType){
			case "oli":
				if(fieldType == "viskositas" || fieldType == "ketahanan"){
					doc[fieldType][value[fieldType]] = value.weight;
				}
				else if(fieldType == "bahan_bakar"){
					doc[fieldType].push(value[fieldType]);
				}
				else{
					if(req.body.dari === ""){
						req.body.dari = 0;
					}
					if(req.body.sampai === ""){
						req.body.sampai = Number.MAX_VALUE;
					}
					doc[fieldType][value[fieldType]] = [req.body.dari, req.body.sampai];
				}
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "cat":
				if(fieldType == "jenis_cat" || fieldType == "warna_cat"){
					doc[fieldType].push(value[fieldType]);
				}
				else{
					doc[fieldType][value[fieldType]] = value.weight;
				}
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "aki":
				doc[fieldType].push(value[fieldType]);
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "minyak_rem":
				doc[fieldType].push(value[fieldType]);
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "cat_vernis":
				doc[fieldType][value[fieldType]] = value.weight;
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "kaca_film":
				doc[fieldType].push(value[fieldType]);
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			case "bohlam":
				doc[fieldType].push(value[fieldType]);
				return utility.put(doc).then(res.redirect('/edit_fields/' + req.params.itemType)).catch(function(err){console.log(err);});
			default:
				break;
		}
	});
});

router.get('/edit_fields', function(req, res, next){
	res.render('edit_fields.ejs', {user:req.session.username});
});

module.exports = router;
